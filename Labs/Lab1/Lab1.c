/*  Name: Patrick Khensovan				*/
/*  Program Name: Lab1.c				*/
/*  Class: CECS 326, Tues/Thurs 5pm 	*/
/*  Date: 9/3/2013						*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[])
{
	int N, i, randomNum, min, max;
	int* randA;
	double average = 0, averageSum = 0;

	srand(time(NULL)); /* Initializes random */

	if(argc != 2)
	{
		printf("We need exactly 2 arguments!\n");
		exit(1);
	}
	
	N = atoi(argv[1]); /* Converts string argument into integer */	
	randA = malloc(N*sizeof(int)); /* Allocates array to N size of int type */

	/*Fills our array with random number*/
	for(i=0; i<N; i++)
	{
		randomNum = rand() % 201 + 1 - 101;
		randA[i] = randomNum;
	}

	min = randA[0];	/*Base cases for max & min*/
	max = randA[0];

	for(i=0; i<N; i++)
	{
		printf("%i ",*(randA+i));
		if(*(randA+i) <= min)
		{
			min = *(randA+i);
		}
		if(*(randA+i) >= max)
		{
			max = *(randA+i);
		}
		averageSum += *(randA+i);
	}
	printf("\n");
	average = (double) (averageSum / N);

	printf("Minimum Number: %i\n", min);
	printf("Maximum Number: %i\n", max);
	printf("Average Number: %2.f\n", average);
	/*Releases from memory*/
	free(randA);
}
