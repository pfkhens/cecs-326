#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	pid_t pid, w; // PID is our process ID, w is our wait
	int k, status, N, T; // N = number of child procsses, T = sleep time
	char value[3];	// the array for our child number below

	// Checks to make sure there are appropriate number of arguments
	if(argc!=3)
	{
		printf("\nPlease give two inputs!\n");
		printf("Usage: %s N T\n", argv[0]);
		printf("Note that N > 0 or N <20, and T >0 or T <50.\n\n");
		exit(1);
	}

	// Assigns our arguments to integer values
	N = atoi(argv[1]); T = atoi(argv[2]);

	// Checks for valid input values for N and T
	if( (N <= 0 || N >= 20) || (T <= 0 || T >= 50) )
	{
		printf("\nN or T are not within range!\n");
		printf("Usage: %s N T\n", argv[0]);
		printf("Note that N > 0 or N <20 and T >0 or T <50.\n\n");
		exit(1);
	}

	for (k = 0; k < N; ++k) // N is our final child number
	{
		if( (pid = fork()) == 0 )
		{
			sprintf(value, "%d", k); // Assigns k to value
			execl("child", "child", value, argv[2] ,(char *) 0);
		}
		else printf("Forked child %d\n", pid);
	}
	// Waits for children
	while ( (w = wait(&status) ) && w != -1 )
	{
		if( w != -1 )
			printf("Wait on PID: %d returns status of: %04X\n", w, status);
	}
	exit(0);
}
