/* pipe.c */
/* Usage: pipe message_to_be_written. Child writes a message to the parent */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>
void main(int argc, char *argv[])
{
	int f_des[2]; 
    int i,k, n; 
    static char message[BUFSIZ]; 
    char buffer[MAX_CANON]; 
    char *c; 
	pid_t childpid;
	if (argc !=2) 
	{ 
		printf ("\n Usage: %s message\n", *argv); exit(1);
	}
	/* generate pipe */
	if (pipe(f_des) == -1) 
	{ 
		perror ("Pipe"); exit(2); 
	}
	switch (fork()) 
	{	// Rather than have the child receive the message, it will send instead
		case -1: perror ("Fork"); exit(3);
		case 0: /* In the child */
			close(f_des[1]);	// This will cause an issue when we try to write.
            if (write(f_des[1], argv[1], strlen(argv[1])) != -1)
            {	// the f_des[1] here will cause us the issue
                printf ("Message sent by parent: [%s]\n", argv[1]); 
                fflush(stdout); 
            }
            else 
            { 
                perror ("Write"); 
                exit(5); 
            }
			break;
		default: /* In the parent */
            close(f_des[0]);	// This will also cause us an issue
			if (read(f_des[0], message, BUFSIZ) != -1) 
			{ 	// f_des[0] is closed, therefore it can't be accessed any more
				printf ("Message received by child: *%s*\n", message); 
				fflush(stdout); 
			}
			else 
			{ 
				perror ("Read"); 
				exit(4);
			}
	}
	exit(0);
}