/* pipe.c */
/* Usage: pipe message_to_be_written. Parent write a message to child */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>
void main(int argc, char *argv[])
{
	int f_des[2]; 
    int i,k, n; 
    static char message[BUFSIZ]; 
    char buffer[MAX_CANON]; 
    char *c; 
	pid_t childpid;
	if (argc !=2) 
	{ 	// If we don't have enough arguments, display error & quit
		printf ("\n Usage: %s message\n", *argv); exit(1);
	}
	/* generate pipe */
	if (pipe(f_des) == -1) 
	{ 	// Pipe error, quits
		perror ("Pipe"); exit(2); 
	}
	switch (fork()) 
	{
		case -1: perror ("Fork"); exit(3);
		case 0: /* In the child */
			close(f_des[1]); 	// Closes the file before we use it
			if (read(f_des[0], message, BUFSIZ) != -1) 
			{	// Reads the file, then flushes it out to buffer
				printf ("Message received by child: *%s*\n", message); 
				fflush(stdout); 
			}
			else 
			{ 	// If we have a read error, we quit
				perror ("Read"); 
				exit(4);
			}
			break;
		default: /* In the parent */
            close(f_des[0]);	// Closes before we can use this
            if (write(f_des[1], argv[1], strlen(argv[1])) != -1)
            {	// If successful, we print out the message
                printf ("Message sent by parent: [%s]\n", argv[1]); 
                fflush(stdout); 
            }
            else 
            { 
                perror ("Write"); 
                exit(5); 
            }
	}
	exit(0);
}