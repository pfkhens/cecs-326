#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>

void main(int argc, char *argv[])
{	// Initialize our variables
	char *fname;	// for file name
	int fd, sleeptime, n_try, count=0;
	pid_t pid;		// for seed
	
	pid = getpid();
	srand((unsigned) pid);
	fname = argv[1];			// 1st arg is file name
	sleeptime = atoi(argv[2]);	// 2nd arg is sleep time
	n_try = atoi(argv[3]);		// 3rd arg is num of tries

	// loops until unlink successfully removes a directory entry
	while( unlink(fname) != 0 )
		if (++count < n_try) sleep(sleeptime);	// sleeps if it exceeds n_try
		else
		{	// otherwise, it is unable to release, and exits
			printf("\nCannot release file\n"); exit(-1);
		}
	printf("\nFile is released\n");
}
