#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>

void main(int argc, char *argv[])
{	// Initialize our variables
	char *fname;	// for file name
	int fd, sleeptime, n_try, count=0;
	pid_t pid;		// for seed
	
	pid = getpid();
	srand((unsigned) pid);
	fname = argv[1];			// 1st arg is file name
	sleeptime = atoi(argv[2]);	// 2nd arg is sleep time
	n_try = atoi(argv[3]);		// 3rd arg is num of tries

	// loops until creation fails, which would mean when creat returns -1
	// errno == EACCES could be any of the following reasons:
	// -process didn't have search permission on a component in pathname
	// -file exists but process didn't have appropriate permissions to open the file
	// -file doesn't exist, and process doesn't have write permission on directory where file is to be created
	while( ( fd = creat(fname, 0) ) == -1 && errno == EACCES )
		if ( ++count < n_try ) sleep(rand() % sleeptime); // sleeps if it exceeds n_try
		else
		{	// otherwise, it is unable to generate, and exits
			printf("\nUnable to generate.\n"); exit(-1);
		}
	close(fd);	// closes the file name
	printf("\nFile %s has been created\n", fname);
}
