#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <stdlib.h>
#define NS 3	// Declares NS

union semun {
	int val; struct semid_ds *buf; ushort *array;
};

int main(void)
{
	int sem_id, sem_value, i; key_t ipc_key; struct semid_ds sem_buf;
	static ushort sem_array[NS] = {3, 1, 4}; union semun arg;
	ipc_key = ftok(".", 'S');

	/* Create semaphore */
	if ((sem_id = semget(ipc_key, NS, IPC_CREAT | 0666)) == -1)	// part A
//	if ((sem_id = semget(ipc_key, NS, IPC_CREAT | IPC_EXCL | 0666)) == -1)	// part C
	{	// 0666 stands for RW RW RW. CREAT creates a new semaphore from a key
		// IPC_EXCL returns an error if it already exists
		perror ("semget: IPC | 0666");
		exit(1);
	}
	printf ("Semaphore identifier %d\n", sem_id);
	/* Set arg (the union) to the address of the storage location for */
	/* returned semid_ds value */

	arg.buf = &sem_buf;	// our buffer
	if (semctl(sem_id, 0, IPC_STAT, arg) == -1)
	{	// SEMCTL success returns 0 or value requested by CMD
		// STAT returns current value of semid_ds structure for semaphore IDer
		// STAT must have read permission, & returned info is stored in user-gen'd structure in 'arg'
		perror ("semctl: IPC_STAT");
		exit(2);
	}
	printf ("Create %d", ctime(&sem_buf.sem_ctime));

	/* Set arg (the union) to the address of the initializing vector */
	arg.array = sem_array;

	if (semctl(sem_id, 0, SETALL, arg) == -1)
	{	// initializes all semaphores in set to values stored in an array
		perror("semctl: SETALL");
		exit(3);
	}
	
	for (i=0; i<NS; ++i)
	{	// returns current value of the individual semaphore
		if ((sem_value = semctl(sem_id, i, GETVAL, 0)) == -1)
		{
			perror("semctl : GETVAL");
			exit(4);
		}
		printf ("Semaphore %d has value of %d\n",i, sem_value);
	}

	/* remove semaphore */ 
/*	if (semctl(sem_id, 0, IPC_RMID, 0) == -1)
	{
		perror ("semctl: IPC_RMID");
		exit(5);
	}*/
} 

