#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

void main(int argc, char* argv[])
{
	if(argc >1)
	{
		// #1, uses the cat command to display the given file in the argument
		execlp("/bin/cat", "cat", argv[1], (char *) NULL);
		// #2, uses the pwd command and displays the current working directory
//		execlp("/bin/pwd", "pwd", argv[1], (char *) NULL);
		// #3, executes the program in the argument
//      execvp(argv[1], &argv[1]);
		// #4, uses cat to display the given text file in the argument
//		static char *new_argv[] = {"cat", "randomText.txt", (char *) 0}; // #4
//		execvp("/bin/cat", new_argv);		// #4
		perror("exec failure");
		exit(1);
	}
	printf("\nUsage: %s test_file\n", *argv);
	exit(1);
}
