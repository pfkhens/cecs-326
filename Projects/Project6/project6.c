/* pipe.c */
/* Usage: pipe message_to_be_written. Parent write a message to child */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#define BUFSIZE 256

/* 
> pipe n pipename c this_is_my_MESSAGE
child 6122 is about to send the message [this_is_my_MESSAGE] to pipename 
message sent
parent is about to read the message from pipename
parent receives the message *THIS_IS_MY_message* from child 6122 
 
> pipe u p this_is_my_MESSAGE
parent is about to send the message [this_is_my_MESSAGE] to FIFO 
message sent 
child 6123 is about to read the message from FIFO 
child 6123 receives the message *THIS_IS_MY_message* from parent

pipe_type indicates type of pipe, ‘n’ for named and ‘u’ for unnamed.
pipe_name is the name of the pipe only if pipe_type is ‘n’; otherwise, pipe_name is not needed.
direction indicates message direction, ‘p’ for parent to child, ‘c’ for child to parent.
message is the message string, limited in size of 32 characters.
*/

void main(int argc, char *argv[])
{
	mode_t fifo_mode = S_IRUSR | S_IWUSR; 
    int fd, status, child; 
    char buf[BUFSIZE]; 
    unsigned strsize;

	int f_des[2]; 
    int i, k, n; 
    static char message[BUFSIZ]; 
    char buffer[MAX_CANON]; 
    char *c; 
	pid_t childpid;

	if (argc !=4) 
	{ 	// If we don't have enough arguments, display error & quit
		printf ("\n Usage: pipe pipe_type pipe_name ");
		printf ("\n Where pipe_type can be 'n' for named or 'u' for unnamed,");
		printf ("\n pipe_name is the name of the pipe if pipe_type is 'n';");
		printf ("\n If 'u' is chosen, pipe_name is not needed.");
		printf ("\n direction indicates message direction, 'p' for parent to child or 
			'c' for child to parent.");
		printf ("\n message indicates the string message with 32 character limit.");
		exit(1);
	}

	if ()
	/* generate pipe */
	if (pipe(f_des) == -1) 
	{ 	// Pipe error, quits
		perror ("Pipe"); exit(2); 
	}
	switch (fork()) 
	{
		case -1: perror ("Fork"); exit(3);
		case 0: /* In the child */
			close(f_des[1]); 	// Closes the file before we use it
			if (read(f_des[0], message, BUFSIZ) != -1) 
			{	// Reads the file, then flushes it out to buffer
				printf ("Message received by child: *%s*\n", message); 
				fflush(stdout); 
			}
			else 
			{ 	// If we have a read error, we quit
				perror ("Read"); 
				exit(4);
			}
			break;
		default: /* In the parent */
            close(f_des[0]);	// Closes before we can use this
            if (write(f_des[1], argv[1], strlen(argv[1])) != -1)
            {	// If successful, we print out the message
                printf ("Message sent by parent: [%s]\n", argv[1]); 
                fflush(stdout); 
            }
            else 
            { 
                perror ("Write"); 
                exit(5); 
            }
	}

	
	/* generate a named pipe with r/w for user */
	if ((mkfifo(argv[1],fifo_mode) == -1) && (errno != EEXIST)) 
	{	// if FIFO file type errors, we quit
		perror ("Pipe"); 
		exit(1); 
	}
	if (( child = fork()) == -1) 	// Fork the children
	{ 
		perror ("Fork"); 
		exit(1); 
	}
	else if (child == 0)
	{ 
		printf ("\n Child %ld is about to open FIFO %s\n", (long)getpid(), argv[1]);
		if ((fd = open(argv[1], O_WRONLY)) == -1) 
		{	// If the FIFO returns -1, we quit
			perror("Child cannot open FIFO"); 
			exit(1);
		}
		/* In the child */
		// sprintf writes to the buffer, then sets the string size
		sprintf (buf, "This was written by child %ld\n", (long)getpid()); 
		strsize = strlen(buf) + 1;
		if (write(fd, buf, strsize) != strsize) 
		{ 	// FIFO fails if the stringsize and write are not the same
			printf("Child write to FIFO failed\n"); 
			exit(1); 
		}
		printf ("Child %ld is done\n", (long)getpid());
	} 
	else 
	{ /* parent does a read */
		printf ("Parent %ld is about to open FIFO %s\n", (long) getpid(), argv[1]);
		if ((fd = open(argv[1], O_RDONLY | O_NONBLOCK)) == -1) 
		{	// Failure to open; quits afterwards
			perror("Parent cannot open FIFO");
			exit(1); 
		}
		printf ("Parent is about to read\n", (long)getpid());
		while ((wait(&status) == -1) && (errno == EINTR));
		
		if (read(fd, buf, BUFSIZE) <=0) 
		{ 
			perror("Parent read from FIFO failed\n");
			exit(1); 
		}
		printf ("Parent %ld received: %s\n", (long)getpid(), buf);
	}
	exit(0);

}

