#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

int main(int argc, char *argv[])
{
    int i, n;
    pid_t childOne, childTwo;
    
    if(argc != 2)    /* Makes sure we have correct inputs */
    {
        printf("\nUsage: %s processes\n", argv[0]);
        exit(1);
    }
    n = atoi(argv[1]);    /* Gets first argument  */
    childOne = 0;         /* Resets the childpid  */
    childTwo = 0;
    /* Tests for valid range. 0<n<10, and 0<=m<20 */
    
    for(i = 0; i<n; i++)
        if(childOne = fork()) {
		if(childTwo = fork()) {
			break; }}
        if(childOne == -1 || childTwo == -1)
        {
            perror("\nThe fork failed\n");
            exit(1);
        }
        printf("\n%d: %6ld %6ld %6ld %6ld", i, (long)getpid(), (long)getppid(), (long)childOne, (long)childTwo);
    exit(0);
}
