#ifndef IO_TEST
#define IO_TEST
/*
author: David Gaskins, Patrick Khensovan
methods:
        isString(char*, char*)
                takes in a string, and makes sure it can be transferred
                to an int with a real value
        checkBound(int, int, int)
                takes in an int, makes sure it is above or below the
                specified bound
        correctArguments(int, int) makes sure we have the number of arguments we are expecting
*/
#include <stdio.h>
int correctArguments(int x, int argc){
        if(x==argc){
                return 1;//do nothing we have correct number of arguments
        }
        else {
                printf("%d is the wrong number of arguments, %d is the correct number.\n", argc, x);
                return(0);
        }
}
int isString(char* a, char* name){
        char* test = "0";
        //!atoi(a) means that it (atoi) returns 0, thus it has 2 options
        //it is "0"(the string containing 0) or another string, thats why we test against "0", if it is not "0" it will return 
        //a non zero number, thus being true
        if(!atoi(a)&& strcmp(test, a)){
                printf("%s can not be a string\n", name);
                return 1;        
        }
        return 0;
}
int inBounds(int x, int bound, int sign, char* name){
        //case 1, check if greater than upper bound
        //case 2, check if less than lower bound
        switch(sign){
        case 1: 
                if(x>bound){
                        printf("%d is greater than %d, the upper bound for %s\n", x, bound, name);
                        return(1);
                }
                break;
        case 2: if(x<bound){
                        printf("%d is less than %d, the lower bound for %s\n", x, bound, name);
                        return(1);
                }
                break;
        }
        return 0;
}
/*'n' or 'r' else fail
NS must match 3 + NS
everything besides argv[1] is not a string, and can't be negative
additions to "iotest.c"*/


//makes sure an input is equal to "n" or "r"
int isNorR(char* test){
        char* test1 = "n";
        char* test2 = "r";
        //if test equals test1 or test2, then we return 1 indicating success
        //else return 0
        if( (strcmp(test, test1)==0) || (strcmp(test, test2)==0) ) 
                return 1;
        return 0;
}
#endif