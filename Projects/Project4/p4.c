#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "iotest.c"
//#define NS 3

union semun {
        int val; struct semid_ds *buf; ushort *array;
};

int main(int argc, char *argv[])
{
        char * ropt; // user options for r or f. probably unnecessary
        int NS, sem_id, sem_value, i;
        ushort * sem_array;

        key_t ipc_key;
        struct semid_ds sem_buf;
        union semun arg;
        ipc_key = ftok(".", 'S');

        //this is the beginning of the many IO checks
        //make if we don't have at least three arguments, we need to stop before checking the arg size
        //later by taking into consideration NS
        if(argc <3){
                printf("incorrect arguments\n");
                exit(0);
        }

        //make sure argv[1] is n or r
        if(!isNorR(argv[1])){
                printf("%s is an invalid value for argv[1]. We allow \"n\" or \"r\" only\n", argv[1]);
                exit(1);
        }

        //check to make sure none of our values that should be numbers are strings
        for(i=2; i<argc;++i){
                if(isString(argv[i], "these numbers")){
                                exit(1);
                }
        }

        //now we check to make sure that our numbers are in the correct range.
        NS = atoi(argv[2]);
        if(NS <= 0)
        {
                printf("Number of semaphores cannot be less than 1.\n");
                exit(0);
        }

        //we are now guaranteed that ns>0 so we can Assign our array size
        sem_array = (ushort*)malloc(NS*sizeof(int)); 
        
        //now we make sure that we have the correct number of arguments also (dynamic) 
        if(argc != (NS+3))
        {
                printf("Arguments do not match!\n");
                exit(0);
        }

        // Assign semaphore to the array
        for(i = 0; i < NS; i++)
        {
                sem_array[i] = atoi(argv[3+i]); // 4 = all arguments + i
        }



        //if we are here, we meant to create a semaphore
        /* Create semaphore */
        if ((sem_id = semget(ipc_key, NS, IPC_CREAT | IPC_EXCL | 0666)) == -1)
        {
                perror ("semget: IPC | 0666");
                exit(1);
        }

        //print identifier

        printf ("Semaphore identifier %d\n", sem_id);
        /* Set arg (the union) to the address of the storage location for */
        /* returned semid_ds value */

        arg.buf = &sem_buf;
        if (semctl(sem_id, 0, IPC_STAT, arg) == -1)
        {
                perror ("semctl: IPC_STAT");
                exit(2);
        }

        printf("Create %s\n", ctime(&sem_buf.sem_ctime));
        /* Set arg (the union) to the address of the initializing vector */
        arg.array = sem_array;
        if (semctl(sem_id, 0, SETALL, arg) == -1)
        {
                perror("semctl: SETALL");
                exit(3);
        }

        for (i=0; i<NS; ++i)
        {
                if ((sem_value = semctl(sem_id, i, GETVAL, 0)) == -1)
                {
                        perror("semctl : GETVAL");
                        exit(4);
                }
                printf ("Semaphore %d has value of %d\n",i, sem_value);
        }
        //if argv[1] is equal to r, then we remove the semaphore
        if(strcmp(argv[1], "r")==0){        
                if (semctl(sem_id, 0, IPC_RMID, 0) == -1)
                {
                        printf("%d\n", sem_id);
                        perror ("semctl: IPC_RMID");
                        exit(5);
                }
                printf("semaphore successfully deleted\n");
                exit(5);
        }
        return 0;
} 