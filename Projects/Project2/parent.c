/**********************************************
 *    Program name: parent.c		      *
 *    CECS 326, Haney Williams		      *
 *    Names: Khensovan, Patrick & Nuon, David *	
 **********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

// Program to check for valid integer input. Will return false if it is not a digit value
int isnumstring(const char* c) 
{
	int out = isdigit(*c) || (*c) == '-'; c++;
	while( (*c) != '\0')
	{
		out &= ( isdigit(*c) );
		c++;
	}

	return out;
}

int main(int argc, char *argv[])
{
	pid_t pid; // w = return value by wait() function call
	int i, T, A, B; // status = status of wait(), waitTime = .. wait time
	int validA, validB, validT;
	char child_num[10]; // child_num is the array for our child number below

	int isValid;

	// Seed PNRG
	srand(pid);

	// checks for user input
	if(argc != 4) {
		printf("%s\nUsage: %s T A B\n", argv[0], argv[0]);
		printf("Where T is in (0, 50] \n");
		printf("Where Operand A is any integer \n");
		printf("Where Operand B is any non-zero integer \n\n");
		exit(1);
	}
	
	// gets our wait time
	// Used for sleep time range
	T = atoi(argv[1]);

	// Operands
	A = atoi(argv[2]);
	B = atoi(argv[3]);
	
	// Input validation
	validA = (isnumstring(argv[2]) == 1);
	validB = (isnumstring(argv[2]) == 1) && (B > 0);
	validT = (isnumstring(argv[2]) == 1) && (T > 0) && (T <= 50);
	isValid  = validA && validB && validT; // May be this... isValid = validA || validB || validT;

	if(!isValid) 
	{
		printf("Invalid input.\n");
		printf("T must be greater than 0 and at most 50, A is any integer, and B is any non-zero integer.\n\n");
		exit(1);
	}
	
	printf("I am the parent process, the maximum sleep time is %d and the two numbers are %d and %d\n",T,A,B);

	// Sleep
	sleep(rand() % T);

	for(i = 0; i<4; ++i)
	{
		switch(pid=fork())
		{
			case 0:
				// in child process, it copies child number to child_num
				sprintf(child_num, "%d", i);
			
				// calls child program, passes current child position + two numbers
				execlp("child", "child", argv[1], child_num, argv[2], argv[3],(char *) 0);
				perror("Exec failure");
				return(1);
			case -1:
				perror("Fork failure.");
				return(2);
			default:
				printf("Forked child %d\n", pid);
		}
	}
	return(0);
}
