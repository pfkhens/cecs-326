/**********************************************
 *    Program name: parent.c		      *
 *    CECS 326, Haney Williams		      *
 *    Names: Khensovan, Patrick & Nuon, David *	
 **********************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[])
{
	pid_t pid;	// Our process ID
	int n, nsleep, num1, num2; // Variables for our passed in arguments
	int sum, difference, product, quotient, remainder; // Our operations

	pid = getpid();	// Gets the process ID for our child. We will use as our seed
	srand(pid);

	// gets our sleep length, sets it
	nsleep = atoi(argv[1]);	// Gets the first argument from the parent
	sleep(rand() % nsleep);	// Sleeps for a random amount of time

	// Passes our arguments into our local variables
	n = atoi(argv[2]); // n determines which function we will use
	
	// Operands
	num1 = atoi(argv[3]);
	num2 = atoi(argv[4]);

	// Using the n value that is passed in, it does the appropriate math + print statements via switch
	switch(n)
	{
		case 0:
			sum = num1 + num2;
			printf("I am child number %d with PID %d, the sum is %d\n", n, pid, sum);
			break;
		case 1:
			difference = num1 - num2;
			printf("I am child number %d with PID %d, the difference is %d\n", n, pid, difference);	
			break;
		case 2:
			product = num1 * num2;
			printf("I am child number %d with PID %d, the product is %d\n", n, pid, product);
			break;
		case 3:
			quotient = num1 / num2;
			remainder = num1 % num2;
			printf("I am child number %d with PID %d, the quotient is %d with remainder %d\n", n, pid, quotient, remainder);
			break;
		default:
			break;
	}
}
