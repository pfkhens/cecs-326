#ifndef IO_TEST
#define IO_TEST
/*
author: David Gaskins, Patrick Khensovan
methods:
	isString(char*, char*)
		takes in a string, and makes sure it can be transferred
		to an int with a real value
	inBounds(int, int, char P, char*)
		takes in an int, makes sure it is above or below the
		specified bound
	correctArguments(int, int) makes sure we have the number of arguments we are expecting

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
    //x is the intsended number of arguments, argc is the actual input from the program
int correctArguments(int x, int argc){
	if(x==argc) {
		return 1;//do nothing we have correct number of arguments
	} else {
        printf("%d is the wrong number of arguments, %d is the correct number.\n", argc, x);
        return(0);
    }
}
//used mostly for determining if numbers that shouldn't be zero, are
//thats why i've hard coded in "can not be a string"
int isString(char* a, char* name){
    char* test = "0";
    //!atoi(a) means that it (atoi) returns 0, thus it has 2 options
    //it is "0"(the string containing 0) or another string, thats why we test against "0", if it is not "0" it will return 
    //a non zero number, thus being true
    if(atoi(a) == 0 && strcmp(test, a) != 0) {
        printf("%s can not be a string\n", name);
        return 1;        
    }
    return 0;
}
/*
Usage:
        argList = [to be checked, checkedAgainst, sign (<, >, =), name of var]
*/
int inBounds(int x, int bound, char sign, char* name) {
    //case 1, check if greater than upper bound
    //case 2, check if less than lower bound
    //case 3, check if equals bound. 
    switch(sign) {
    case '<':
        if(x<bound) {
            printf("%d is less than %d, the lower bound for %s\n", x, bound, name);
            return(1);
        }
        break;
    case '>': 
        if(x>bound) {
            printf("%d is greater than %d, the upper bound for %s\n", x, bound, name);
            return(1);
        }
        break;
    case '=': if(x !=bound) {
            printf("%d does not equal the variable %s (value: %d)\n", x, name, bound);
            return(1);
            }
        break;
    }
    return 0;
}
/*'n' or 'r' else fail
NS must match 3 + NS
everything besides argv[1] is not a string, and can't be negative
additions to "iotest.c"*/


//makes sure an input is equal to "n" or "r"
int isNorS(char* test){
    char* test1 = "n";
    char* test2 = "s";
    //if test equals test1 or test2, then we return 1 indicating success
    //else return 0
    if( (strcmp(test, test1)==0) || (strcmp(test, test2)==0) ){
        return 0;        
    }
    printf("the input is not \"n\" or \"s\"\n");
    return 1;
}
#endif