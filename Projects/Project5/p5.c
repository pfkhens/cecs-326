//NOTES: Creates N child procs
//    Each child -
//        Writes child number
//            CHILD PID
//            Parent PID
//            Child PID in BUFFER
//    Create critical section where CHILD PROC displays BUFFER CONTENTS
//    
//    Create SEMAPHORE to PROTECT CRITICAL SECTION
//    
//    Use DELAY ADJUSTMENT (k) in CS to adjust speed of display process
//        -To show that displayed contents are randomly interleaved w/o SEMAPHORE PROTECTION
//    
//    EXAMPLE OUTPUT:
//    > p 3 n 200       /* NOTE HOW THEY RUN INTO EACH OTHER AND INTERRUPT */
//        i: 3 pi: 1 processi:2 process ID:4084 parent ID:4049 child ID:40 ID:4085 parent ID:4084 child ID:0
//        rocess ID:4086 parent ID:4084 child ID:086
//    > p5 3 s 200
//        i: 1 process ID:4087 parent ID:4049 child ID:4088
//        i: 2 process ID:4088 parent ID:4087 child ID:4089
//        i: 3 process ID:4089 parent ID:4088 child ID:0
//    ALSO:
//        semaphore N opt k
//    where:
//        N: number of child processes (integer)
//        opt: option: “n” 􀃆 no semaphore protection, “s” 􀃆 with semaphore protection
//        k: delay adjustment parameter (integer)
//            
//    HINTS:
//1) Function to initialize the struct sembuf structure members sem_num, sem_op, and sem_flg in an
//implementation-independent manner:
//Prototype:
//    void set_sembuf_struct(struct sembuf *s, int semnum, int semop, int semflg);
//    void set_sembuf_struct(struct sembuf *s, int num, int op, int flg)
//    {
//        s-> sem_num = (short) num;
//        s-> sem_op = op;
//        s-> sem_flg = flg;
//        return;
//    }
//2) Implement wait() and signal()
//    struct sembuf semwait[1];
//    struct sembuf semsignal[1];
//    /* Initialize semaphore element to 1 */
//    set_sembuf_struct(semwait, 0, -1, 0);
//    set_sembuf_struct(semsignal, 0, 1, 0);
//    if (semop(semid, semsignal, 1) == -1) 
//    {
//        printf ("%ld: semaphore increment failed - %s\n", (long)getpid(), strerror(errno));
//        if (semctl(semid, 0, IPC_RMID) == -1)
//            printf ("%ld: could not delete semaphore - %s\n", (long)getpid(), strerror(errno));
//        exit(1);
//    }
//3) Entry section:
//        while (( (semop_ret = semop(semid, semwait, 1) ) == -1) && (errno ==EINTR));
//            if (semop_ret == -1)
//                printf ("%ld: semaphore decrement failed - %s\n", (long)getpid(), strerror(errno));
//            else
//4) Exit section:
//    while (((semop_ret = semop(semid, semsignal, 1)) == -1) && (errno == EINTR));
//        if (semop_ret == -1)
//        printf ("%ld: semaphore increment failed - %s\n", (long)getpid(), strerror(errno));

#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>
#include <stdlib.h>

/* NOT QUITE SURE IF THESE BELONG HERE, BUT HERE ARE THE STRUCTS DAWG */
void set_sembuf_struct(struct sembuf *s, int semnum, int semop, int semflg);

void set_sembuf_struct(struct sembuf *s, int num, int op, int flg)
{
    	s-> sem_num = (short) num;
    	s-> sem_op = op;
    	s-> sem_flg = flg;
    	return;
}

int main(int argc, char *argv[])
{
//  SAMPLE INPUT
//        semaphore N opt k
//    where:
//        N: number of child processes (integer)
//        opt: option: “n” 􀃆 no semaphore protection, “s” 􀃆 with semaphore protection
// 	  k: delay adjustmt parameter (integer)
    	int N, i, k;
    	char * opt; // user options for r or f

	key_t ipc_key;
	pid_t parent, child;
	ipc_key = ftok(".", 'S');
	
	// Our buffers
	struct sembuf semwait[1]; 
	struct sembuf semsignal[1]; 
/*
        int NS, sem_id, sem_value, i;
        ushort * sem_array;

        union semun arg;
*/
	n = atoi(argv[1]);
        opt = argv[2];
        k = atoi(argv[3]);

	// Our buffers
/*	struct sembuf semwait[1]; 
	struct sembuf semsignal[1]; 	*/

	/* Initialize semaphore element to 1 */ 
	set_sembuf_struct(semwait, 0, -1, 0); 
	set_sembuf_struct(semsignal, 0, 1, 0); 
 
	if (semop(semid, semsignal, 1) == -1) 
	{
		printf ("%ld: semaphore increment failed - %s\n", (long)getpid(), strerror(errno)); 
		if (semctl(semid, 0, IPC_RMID) == -1) 
			printf ("%ld: could not delete semaphore - %s\n", (long)getpid(), strerror(errno)); 
		exit(1);
	}


	//Entry section:
    	while (( (semop_ret = semop(semid, semwait, 1) ) == -1) && (errno ==EINTR));
        	if (semop_ret == -1)
	        	printf ("%ld: semaphore decrement failed - %s\n", (long)getpid(), strerror(errno));
        	else
            	// Do something

	// Now in the critical section
	// We should be printing out here... but first I need mah childrenz....
    
	// Exit section
	while (((semop_ret = semop(semid, semsignal, 1)) == -1) && (errno == EINTR));
        	if (semop_ret == -1)
        		printf ("%ld: semaphore increment failed - %s\n", (long)getpid(), strerror(errno));
    
}
