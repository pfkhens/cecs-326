#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	pid_t pid, w; // PID is our process ID, w is our wait
	int k, status, num_tries, sleeptime, count=0; // N = number of child procsses, T = sleep time
	char value[3];	// the array for our child number below

	// Assigns our arguments to integer values
	num_tries = atoi(argv[1]); sleeptime = atoi(argv[2]);

    int ret_value = (int) (pid % 256);
    
	for (k = 0; k < 3; ++k) // N is our final child number
	{
		if( (pid = fork()) == 0 )
		{
			sprintf(value, "%d", k); // Assigns k to value
            count = 0;
            
            // parent 3 3 text.dat 
            // [1] == tries
            // [2] == sleep
            // [3] == text.dat
            // char* string == 
            while(++count < num_tries)
            {
                if(execl("./acquire", "acquire", "lock1", 1, 1,(char *) 0)); // if (acquire is successful)
                {    
//                    execl("/bin/cat", "cat", argv[3], (char *) NULL);
//                    execl("release", "release", "lock1", 1,1, (char *) NULL);
                    
                    //exit system, return value equal to least significant 8 bits of PID
                    printf("Returns status of %04X\n", ret_value);
                }
                execl("release", "release", "lock1", 1, 1, (char *) 0);
//                else {                    sleep(rand() % sleeptime);                }
            }
        }

//            if(doesn't aqcquire)
//                printf("\nUnable to generate.\n");

        // Waits for children
        while ( (w = wait(&status) ) && w != -1 )
        {
            if( w != -1 )
                printf("Wait on PID: %d returns status of: %04X\n", w, status);
        }
        exit(0);
    }
}