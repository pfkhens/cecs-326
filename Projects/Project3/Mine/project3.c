#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>

void main(int argc, char *argv[])
{
	pid_t pid, w; // PID is our process ID, w is our wait
	int k, status, N, T; // N = number of child procsses, T = sleep time
	char value[3];	// the array for our child number below

	// Assigns our arguments to integer values
	N = atoi(argv[1]); T = atoi(argv[2]);

	for (k = 0; k < 3; ++k) 
	{
		if( (pid = fork()) == 0 )
		{
			sprintf(value, "%d", k); // Assigns k to value
			execl("acquire", acquire, lock,(char *) 0);
		}
		else printf("Forked child %d\n", pid);
	}    
}