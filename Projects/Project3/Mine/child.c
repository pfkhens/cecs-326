#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	pid_t pid; // pid is our process ID
	int ret_value, T; // assigns value to return, and sleep time

	// Assigns our process ID values, also sets the value to return
	pid = getpid();
	ret_value = (int) (pid %256);

	T = atoi(argv[2]); // Assigns our maximum sleep time & sleeps
	sleep(rand() % T);

	// Prints and then kills the process if this condition is satisfied
	if (atoi(*(argv + 1)) %2)
	{
		printf("Child %d is terminating with signal 009\n", pid);
		kill(pid, 9);
	}
	else
	{	// Terminates our program, then returns the value created above
		printf("Child %d is terminating with exit(%04X)\n", pid, ret_value);
		exit(ret_value);
	}
}
