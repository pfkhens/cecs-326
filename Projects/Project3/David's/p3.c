#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include "iotest.c"
void main(int argc, char* argv[]){
        char* filename;
        char* lockfileName = "lock1";
        
        pid_t pid, pid_c, w;
        //check to see number of arguments
        if(!correctArguments(4, argc))
                exit(1);
        int num_tries, sleeptime, k, status, i;
        filename = argv[1];
        if(strcmp("text.dat", filename)) {
                printf("file needs to be named \"text.dat\"\n");
                exit(1);
        }
        if(isString(argv[2], "num_tries")) exit(1);
        num_tries = atoi(argv[2]);
        checkBound(num_tries, 1, 2, "num_tries");
        if(isString(argv[3], "sleeptime")) exit(1);
        sleeptime = atoi(argv[3]);
        checkBound(sleeptime, 1, 2, "sleeptime");
        unlink(lockfileName);
        for(k = 0; k<3; ++k){
                //make the three children
                if((pid = fork())==0){
                        //tests acquire
                        for(i = 0; i<num_tries;++i){
                                if((pid_c =fork())== 0){
                                //have a child exit 
                                        char sleeptimeS[3];
                                        sprintf(sleeptimeS, "%d", sleeptime);
                                        execlp("acquire", "acquire", lockfileName, sleeptimeS, "1", (char*) NULL);
                                }
                                while ((w = wait(&status)) && w != - 1) { 
                                                if (w != - 1){
                                                        //do nothing, but rather know that acquire has been run
                                                }
                                }
                                //printf("status: %d from child %d\n", status, k);
                                if(status==0)
                                        break;
                                else {
                                        //printf("Sleepy time!\n");
                                        sleep(rand()%sleeptime);
                                }
                        }
                        if(i==num_tries){
                                //no lock file
                                printf("unable to obtain lockfile\n");
                                exit(k);
                        }
                        //now we have either made or not made a lock file
                        //if we haven't this child has quit
                        //if we have made a log file, we can continue
                        //now we make a child to exec cat
                        if((pid_c = fork())==0){
                                execlp("/bin/cat", "/bin/cat", filename, (char*) NULL);
                        }
                        //now we wait to see if that is done
                        while ((w = wait(&status)) && w != - 1){
                                if(w!=-1){//printf("cat has executed\n");
                                }
                        }
                        //we don't care about the status of cat
                        //now we make a child to release the lock file
                        if((pid_c =fork())== 0){
                                        char sleeptimeS[3];
                                        sprintf(sleeptimeS, "%d", sleeptime);
                                        execlp("release", "release", lockfileName, sleeptimeS, "1", (char*)NULL);
                        }
                        while ((w = wait(&status)) && w != - 1) { 
                                if (w != - 1){
                                        //release has been run
                                }
                        }
                        //if(status==0)
                                //printf("successful release\n");
                        exit(getpid()&255);
                }
                else{
/*                        
                        while ((w = wait(&status)) && w != - 1) { 
                                if (w != - 1){
                                        printf("Wait on PID: %d returns status of: %04X\n", w, status);
                                }
                        }*/
                        //printf("forked child %d\n", pid);
                }
        }
        while ((w = wait(&status)) && w != - 1) { 
                                if (w != - 1){
                                        printf("Wait on PID: %d returns status of: %04X\n", w, status);
                                }
        }
        exit(0);
}