#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include "iotest.c"
void main(int argc, char *argv[]) 
{ 
        char *fname;
        int fd, sleeptime, n_try, count=0;
        pid_t pid; 
        pid = getpid();
        srand((unsigned) pid);
        //any file name is valid, including numbers
        //so don't check file name
        fname = argv[1];
        //make sure sleep time (ie argv[2]) is a number
        if(isString(argv[2], "sleeptime")) exit(1);
        sleeptime = atoi(argv[2]);
        //make sure sleep time is greater than 0
        checkBound(sleeptime, 1, 2, "sleeptime");
        //make sure n_try (ie argv[3] ) is a number also
        if(isString(argv[3], "n_try")) exit(1);
        n_try = atoi(argv[3]); 
        //make sure n_trys is also greater than 0
        checkBound(n_try, 1, 2, "n_try");
        while (unlink(fname)!=0)
                if (++count < n_try) sleep(sleeptime); 
                else { 
                        //printf ("\n Cannot release file\n"); 
                        exit(-1); 
                } 
        //printf ("\nFile is released\n"); 
        exit(0);
} 